# Bitcoin Please Contracts

Official repository for ALL Bitcoin Please EVM-compatible contracts.

__Our supporting networks include:__

1. SmartBCH | SBCH

## BCHPleaseDb

This is an __[Eternal Database](https://blog.colony.io/writing-upgradeable-contracts-in-solidity-6743f0eecc88)__ that provides a sustainable storage solution for use throughout the upgrade lifecycle of managing contracts.

_NOTE: We've deployed these databases to ALL of our supporting networks._

__SmartBCH | SBCH__

- Mainnet - __[0x1D0847fB7f168a6011D756F5F3835E0acfA7F1fd](https://smartscan.cash/address/0x1D0847fB7f168a6011D756F5F3835E0acfA7F1fd#code)__
- Amber - __[0x806f9f901EB26A208c2b88Fc36C7Cc8acc6fb45e](https://smartscan.cash/address/0x806f9f901EB26A208c2b88Fc36C7Cc8acc6fb45e#code)__

## EC Recovery

`bytes32 hash = keccak256('aname.ecrecovery');`

`0xbd98df5a38b17f6731f93b24df589bf48b6a005843651676982488e97cdfcc3d`

- Mainnet - __[0xbe396F73C10Ca38c47dBFB116d37De913AeCB854](https://smartscan.cash/address/0xbe396F73C10Ca38c47dBFB116d37De913AeCB854#code)__
- Amber - __[0x3431EcBe6869117A7FBeb577b4fcA40Eb7aDe845](https://smartscan.cash/address/0x3431EcBe6869117A7FBeb577b4fcA40Eb7aDe845#code)__
